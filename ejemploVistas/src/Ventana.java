import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField InputNombre;
	private JTextField InputApellido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Formulario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 220, 300);							//Seteo de los limites de la ventana x,y del tama�o, x1, y1 posicion en la pantalla
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		InputNombre = new JTextField();
		InputNombre.setBounds(108, 111, 86, 20);
		contentPane.add(InputNombre);
		InputNombre.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setLabelFor(InputNombre);
		lblNombre.setBounds(10, 111, 71, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(10, 158, 71, 14);
		contentPane.add(lblApellido);
		
		InputApellido = new JTextField();
		lblApellido.setLabelFor(InputApellido);
		InputApellido.setBounds(108, 158, 86, 20);
		contentPane.add(InputApellido);
		InputApellido.setColumns(10);
		
		JButton btnNewButton = new JButton("Saludar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saludar();
			}
		});
		btnNewButton.setBounds(49, 210, 89, 23);
		contentPane.add(btnNewButton);
		
		
		JLabel Icono = new JLabel("");
		//creamos un icono, que le pasamos como parametro otro icono que tiene una imagen reescalada
		ImageIcon imageIcon = new ImageIcon(new ImageIcon("img\\logo.png").getImage().getScaledInstance(75,81, Image.SCALE_DEFAULT));
		//seteamos el icono
		Icono.setIcon(imageIcon);
		Icono.setBounds(65, 11, 90, 77);
		contentPane.add(Icono);
		
		setLocationRelativeTo(null);	//la ventana se abre en el medio
	}

	protected void saludar() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(this, "hola " + InputNombre.getText() + " " + InputApellido.getText());
	}
}
