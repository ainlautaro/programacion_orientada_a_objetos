package actividadSemana1;

//Comentario de una linea

/*
*comentario de multiples lineas
*/

/**
* COMENTARIO UTILIZADO EN JAVADOCS
* 
* /* OPERADORES LOGICOS: igualdad: == desigualdad o distinto: != menor: <
* mayor: > menor o igual: <= mayor o igual: >=
*/

/*
* OPERADPRES: &: and &&: AND, si la primera es falsa, ya no se evalua la
* segunda |: or ||: OR, si primera es verdadera, ya no evalua la segunda ^: XOR
*/

/*
* OPERADORES DE ACCION COMPUESTOS:
* 
*/

public class Main {

	public static void main(String[] args) {

		// ----------------------Variables----------------------//
		int Entero = 5; //ojo los atributos y variables van en minuscula
		double Flotante = 2.5; // double utiliza 64bits float utiliza 32bits //ojo los atributos y variables van en minuscula
		boolean booleano = true;
		char caracter = 'A';
		char caracter2 = 'B';

		// ----------------------Conversion de Datos----------------------//
		// para convertir un tipo de variable a otro esta se debe "castear"
		int casteo = (int) Flotante; // la pasar de double a int, se produce una perdida de datos(parte decimal) //ojo los atributos y variables van en minuscula

		// ----------------------Instrucciones Escenciales----------------------//

		// If (Condicional)
		System.out.println("");
		System.out.println("-----------------------Instrucciones Escenciales-----------------------");
		System.out.println("");

		System.out.println("*If's:");
		if (Entero == Flotante) {
			System.out.println(" .If comun: Son iguales");
		} else {
			System.out.println(" .If else Comun: " + Entero + " no es igual a " + Flotante);
		}

		// ----------------------OPERADORES CORTOS----------------------//
		System.out.println(Entero > Flotante ? " .If Corto: numeroEntero > numeroFlotante"
				: "If Corto: numeroEntero < numeroFlotante");// If corto

		int resultado = Entero != 0 ? 100 / Entero : 0; // asignacion con condicion
		System.out.println("");
		System.out.println("* Asignado con condicion: " + resultado);

		// ----------------------SWICH----------------------//
		System.out.println("");
		System.out.print("* Switch: ");
		switch (caracter) {
		case 'A':
			System.out.println("El caracter es: " + caracter);
			break;
		case 'B':
			System.out.println("El caracter es: " + caracter2);
			break;
		default:
			System.out.println("No hay coincidencia");
			break;
		}

		// ----------------------BUCLES----------------------//
		System.out.println("");
		System.out.println("*Bucles:");
		int[] arregloEnteros = { 1, 2, 3, 4, 5 }; // Arreglo estatico

		// While
		int i = 0; // Variable temporal
		System.out.print(" .Recorrido arreglo con While: ");
		while (i < arregloEnteros.length) { // condicion de repeticio: i < longitud del arreglo
			System.out.print(arregloEnteros[i] + " ");
			i++; // incremento i
		}

		// Do While (Repetir)
		int j = 4;
		System.out.print("\n .Recorrido arreglo con Do While: ");
		do {
			System.out.print(arregloEnteros[j] + " ");
			j--; // Decremento j
		} while (j >= 0);

		// For
		System.out.print("\n .Recorrido arreglo con For: ");
		for (int k = 0; k < arregloEnteros.length; k++) {
			System.out.print(arregloEnteros[k] + " ");
		}

		// Recorrido Matriz
		int[][] matrizEnteros = new int[2][2]; // Matriz de n�meros enteros que supondremos llena.
		// 2 filas y 2 columnas.

		System.out.println("\n .Recorrido matriz con For: ");
		for (int m = 0; m < matrizEnteros.length; m++) {
			for (int n = 0; n < matrizEnteros.length; n++) {
				matrizEnteros[m][n] = Entero++;
			}
		}
		for (int m = 0; m < matrizEnteros.length; m++) {
			for (int n = 0; n < matrizEnteros.length; n++) {
				System.out.print("\t" + matrizEnteros[m][n]);
			}
			System.out.print("\n");
		}

		// ----------------------IMPRIMO LAS VARIABLES----------------------//
		System.out.println("\n-----------------------Variables-----------------------");
		System.out.println("\n* Numero Entero: " + Entero);
		System.out.println("* Numero Flotante: " + Flotante);
		System.out.println("* Dato casteado (convertido de decimal a entero): " + casteo);
		System.out.println("* Booleano: " + booleano);
		System.out.println("* Caracteres: " + caracter + ", " + caracter2);

	}

}
