import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JToolBar;
import javax.swing.JScrollBar;
import java.awt.Panel;
import javax.swing.UIManager;
import java.awt.Window.Type;

public class Visual extends JFrame {

	private JPanel contentPane;
	private JLabel Icono;
	private JTextField inputURL;
	private JLabel lblUrl_Imagen;
	private JLabel tituloVentana;
	private JLabel lblObligatorio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visual frame = new Visual();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Visual() {
		setType(Type.POPUP);
		setBackground(new Color(102, 153, 255));
		setTitle("Visualizador de Imagenes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 323);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("menu"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//-------------------------------------------------------------------------------OBJETO DONDE SE MOSTRARA LA IMAGEN
		Icono = new JLabel("");
		Icono.setBackground(UIManager.getColor("info"));
		Icono.setBounds(90, 45, 260, 140);
		ImageIcon imageIcon = new ImageIcon(new ImageIcon("img\\fondo.jpg").getImage().getScaledInstance(Icono.getWidth(),Icono.getHeight(), Image.SCALE_DEFAULT));	
		//seteamos el icono
		Icono.setIcon(imageIcon);
		contentPane.add(Icono);
		
		//-------------------------------------------------------------------------------INPUT DEL URL DE LA IMG
		inputURL = new JTextField();
		inputURL.setBounds(177, 208, 247, 20);
		contentPane.add(inputURL);
		inputURL.setColumns(10);
		
		//-------------------------------------------------------------------------------LBL DEL INPUT DE LA IMG - "Ingrese la URL de su imagen"
		lblUrl_Imagen = new JLabel("Ingrese la URL de su imagen");
		lblUrl_Imagen.setLabelFor(inputURL);
		lblUrl_Imagen.setBounds(10, 208, 166, 17);
		contentPane.add(lblUrl_Imagen);
		
		//-------------------------------------------------------------------------------BOTON "Visualizar", con "escuchador" que llamara a un method cuando se pulse
		JButton btnNewButton = new JButton("Visualizar");
		btnNewButton.setBackground(new Color(255, 99, 71));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setToolTipText("Boton para Visualizar img");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				visualizar();
			}
		});
		
		btnNewButton.setBounds(316, 239, 108, 23);
		contentPane.add(btnNewButton);
		
		//-------------------------------------------------------------------------------PANEL CONTENEDOR TITULO
		Panel contenedorTitulo = new Panel();
		contenedorTitulo.setBackground(new Color(255, 99, 71));
		contenedorTitulo.setBounds(0, 0, 434, 39);
		contentPane.add(contenedorTitulo);
		contenedorTitulo.setLayout(null);
		
		//-------------------------------------------------------------------------------TITULO EN LA PESTANIA "VISUALIZADOR DE IMAGENES"
		tituloVentana = new JLabel("VISUALIZADOR DE IMAGENES");
		tituloVentana.setBounds(104, 11, 226, 19);
		tituloVentana.setForeground(Color.WHITE);
		contenedorTitulo.add(tituloVentana);
		tituloVentana.setHorizontalAlignment(SwingConstants.CENTER);
		tituloVentana.setFont(new Font("Tahoma", Font.BOLD, 15));
		tituloVentana.setBackground(Color.WHITE);
		
		//-------------------------------------------------------------------------------LBL "* Obligatorio", SE MUESTRA SI EL INPUT ESTA VACIO
		lblObligatorio = new JLabel("* Obligatorio");
		lblObligatorio.setLabelFor(inputURL);
		lblObligatorio.setBounds(177, 230, 81, 14);
		contentPane.add(lblObligatorio);
		lblObligatorio.setVisible(false);
	
		setLocationRelativeTo(null);	//la ventana se abre en el medio
	}

	private void visualizar() { 
		
		if (inputURL.getText().equals("")) {
			lblObligatorio.setVisible(true);
		}else {
			lblObligatorio.setVisible(false);
			//creamos un icono, que le pasamos como parametro otro icono que tiene una imagen reescalada
			ImageIcon imageIcon = new ImageIcon(new ImageIcon(inputURL.getText()).getImage().getScaledInstance(Icono.getWidth(),Icono.getHeight(), Image.SCALE_DEFAULT));	
			//seteamos el icono
			Icono.setIcon(imageIcon);
		}
		
	}
}

/*
 muy bueno el programa, ademas esta ordenado, se nota que analizaste bien el codigo autogenerado. 
 ninguna observación, quizas el protected del visualizar podria ser private, ya que no se usa en otra clase. 
 */
