/*
 * Constructor del objeto venta (producto que se vendera)
 */
public class DetallesVenta {
	
	private String producto;
	private Integer cantidad;
	
	//constructor
	public DetallesVenta(String producto, Integer cantidad) {
		super();
		this.setProducto(producto);
		this.setCantidad(cantidad);
	}

	//getters and setters
	public final String getProducto() {
		return producto;
	}

	public final void setProducto(String producto) {
		this.producto = producto;
	}

	public final Integer getCantidad() {
		return cantidad;
	}

	public final void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
