/*
 * constructor que creara al objeto donde guardaremos los datos del json
 */

import java.time.LocalDate;

public class Datos {
	
	private LocalDate fecha;
	private Double cambio;
	
	public Datos(LocalDate fecha, Double cambio) {
		super();
		this.fecha = fecha;
		this.cambio = cambio;
	}

	public final LocalDate getFecha() {
		return fecha;
	}

	public final void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public final Double getCambio() {
		return cambio;
	}

	public final void setCambio(Double cambio) {
		this.cambio = cambio;
	}
		
	

}
