/*
 * Aqui realizaremos la conversion de valores
 */

import java.time.LocalDate;
import java.util.ArrayList;

public class Cambio {
	
	private ArrayList<Datos> cambio = new ArrayList<>();
	
	public Cambio(Datos dato) {
		super();
		this.getCambio().add(dato);
	}
	
	public ArrayList<Datos> getCambio(){
		return cambio;
	}
	
	public void setCambio(ArrayList<Datos> cambio) {
		this.cambio = cambio;
	}

	public void impPromedio() {
		Double total = 0.0;
		
		for (Datos datos : cambio) {
			total = total + datos.getCambio();
		}
		
		System.out.println("Promedio = " + total/this.getCambio().size());
	}
	
	public void fechas(LocalDate desde, LocalDate hasta) {
		for (Datos datos : cambio) {
			if (datos.getFecha().equals(desde) || datos.getFecha().equals(hasta) || datos.getFecha().isBefore(hasta) && datos.getFecha().isAfter(desde)) {
				System.out.println("Fecha = " + datos.getFecha() + " Valor = " + datos.getCambio());
			}
		}
	}
}
