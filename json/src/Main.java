import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//un json, es un archivo que se utiliza para intercambiar grandes volumenes de datos	

public class Main {

	public static void main(String[] args) {
		
		//creamos un objeto
		DetallesVenta venta = new DetallesVenta("Botella de agua", 20);
		
		//creamos un objeto que utilice las clases LocalDateSerializer o LocalDateDeseralizer
		GsonBuilder gsonbuilder = new GsonBuilder();
		//registramos un adaptador a la libreria, el primer parametro es la clase y el segundo el objeto
		gsonbuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
		//registramos un adaptador a la libreria, el primer parametro es la clase y el segundo el objeto
		gsonbuilder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
		
		//creamos un objeto que utilice esto
		Gson gson = gsonbuilder.setPrettyPrinting().create();
		
		//objeto de tipo gson
		//creamos una variable y le asignamos el objeto, para crear un json
		//Gson gson = new Gson();
		//String json = gson.toJson(venta);
		//System.out.println(json);
		
		
		//pasamos un json a un objeto
		//DetallesVenta venta2 = gson.fromJson(json, DetallesVenta.class);
		//System.out.println(venta.getProducto()); se imprime el producto
		
		
		//---------------------------------Peticion lttp a una pagina web---------------------------------//
		
		//1)Declaramos una var de tipo URL
		URL url;
		
		try {
			//2)A la var de tipo URL le asignamos la URL a utilizar
			url = new URL("https://raw.githubusercontent.com/alevega/datosCambio/master/datos.json");
			
			//3)Abro una conexion a esa url
				URLConnection conexion = url.openConnection();
				
			//4)Leemos el contenido de la url
				BufferedReader lectura = new BufferedReader(new InputStreamReader(conexion.getInputStream())); //crea un buffer de lectura, con los datos de la url obtenidos
			
			//5)Mostramos los datos
				String datos = "";
				String linea;
				
				//5.1)Asignamos las lineas de datos mientras el contenido no este vacio
				while ((linea = lectura.readLine()) != null) {
					datos = datos + linea;
				}
				
				//System.out.println(datos);
				
				//6)pasamos el json a un objeto
				Cambio cam = gson.fromJson(datos, Cambio.class);
				
				//imprimos el objetos
				//System.out.println(cam.getCambio().get(0).getFecha());
				cam.impPromedio();
				cam.fechas(LocalDate.of(2018, 7, 01), LocalDate.of(2018, 7, 31));
				
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
