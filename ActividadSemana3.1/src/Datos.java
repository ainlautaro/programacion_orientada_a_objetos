import java.time.LocalDate;

public class Datos {
	
	private LocalDate fecha;
	private Double cambio;
	
	//constructor
	public Datos(LocalDate fecha, Double cambio, Double saldo) {
		super();
		this.fecha = fecha;
		this.cambio = cambio;
	}

	public final LocalDate getFecha() {
		return fecha;
	}

	public final void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public final Double getCambio() {
		return cambio;
	}

	public final void setCambio(Double cambio) {
		this.cambio = cambio;
	}	

}
