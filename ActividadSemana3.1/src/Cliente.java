import java.security.SecureRandom;

public class Cliente {

	//atributos de la clase
	private String nombre, apellido;
	private Double cuentaPesos, cuentaDolares;
	
	//constructor sobrecargado
	public Cliente(String nombre, String apellido, Double cuentaPesos, Double cuentaDolares) {
		super();
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setCuentaPesos(this.cuentaPesosInicial());
		this.setCuentaDolares(cuentaDolares);;
	}
	
	//metodo para generar inicio cuenta Pesos
	private Double cuentaPesosInicial() {
		SecureRandom rnd = new SecureRandom();
		Double num = rnd.nextDouble() + 1000;
		return num;
	}

	public final String getNombre() {
		return nombre;
	}
	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public final String getApellido() {
		return apellido;
	}	
	public final void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public final Double getCuentaPesos() {
		return cuentaPesos;
	}
	public final void setCuentaPesos(Double cuentaPesos) {
		this.cuentaPesos = cuentaPesos;
	}
	public final Double getCuentaDolares() {
		return cuentaDolares;
	}
	public final void setCuentaDolares(Double cuentaDolares) {
		this.cuentaDolares = cuentaDolares;
	}

	

}
