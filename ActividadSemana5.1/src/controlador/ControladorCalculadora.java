package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vista.PantallaCalculadora;

public class ControladorCalculadora implements ActionListener{

	private PantallaCalculadora vistaCalculadora;

	public ControladorCalculadora() {
		super();
		this.vistaCalculadora = new PantallaCalculadora(this);
		
		this.vistaCalculadora.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	//---------------------------GETTERS Y SETTERS---------------------------//
	public PantallaCalculadora getVistaCalculadora() {
		return vistaCalculadora;
	}

	public void setVistaCalculadora(PantallaCalculadora vistaCalculadora) {
		this.vistaCalculadora = vistaCalculadora;
	}
	
}
