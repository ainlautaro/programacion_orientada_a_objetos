package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorCalculadora;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Font;

public class PantallaCalculadora extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	private JButton btnPunto;
	private ControladorCalculadora controlador;
	private JButton btnModulo;
	private JButton btnEliminarActual;
	private JButton btnEliminarOperacion;
	private JButton btnBorrar;
	private JButton btnDerivada;
	private JButton btnPotenciaCuadrada;
	private JButton btnRaizCuadrada;
	private JButton btnDivision;
	private JButton btnSiete;
	private JButton btnOcho;
	private JButton btnNueve;
	private JButton btnMultiplicacion;
	private JButton btnCuatro;
	private JButton btnCinco;
	private JButton btnSeis;
	private JButton btnResta;
	private JButton btnUno;
	private JButton btnDos;
	private JButton btnTres;
	private JButton btnSuma;
	private JButton btnNegar;
	private JButton btnCero;
	private JButton btnIgual;

	/**
	 * Create the frame.
	 */
	public PantallaCalculadora(ControladorCalculadora controlador) {
		this.setControlador(controlador);
		
		setTitle("Calculadora");
		setBackground(Color.DARK_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 340, 540);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(34, 34, 34));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);
		
		//--------------------------------------------------NUMEROS--------------------------------------------------//
		btnSiete = new JButton("7");
		btnSiete.setFont(new Font("Arial", Font.BOLD, 15));
		btnSiete.setBackground(Color.BLACK);
		btnSiete.setForeground(Color.WHITE);
		btnSiete.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnSiete.setBounds(10, 284, 75, 50);
		contentPane.add(btnSiete);
		
		btnOcho = new JButton("8");
		btnOcho.setFont(new Font("Arial", Font.BOLD, 15));
		btnOcho.setBackground(Color.BLACK);
		btnOcho.setForeground(Color.WHITE);
		btnOcho.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnOcho.setBounds(87, 284, 75, 50);
		contentPane.add(btnOcho);
		
		btnNueve = new JButton("9");
		btnNueve.setFont(new Font("Arial", Font.BOLD, 15));
		btnNueve.setBackground(Color.BLACK);
		btnNueve.setForeground(Color.WHITE);
		btnNueve.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnNueve.setBounds(164, 284, 75, 50);
		contentPane.add(btnNueve);
		
		btnCuatro = new JButton("4");
		btnCuatro.setFont(new Font("Arial", Font.BOLD, 15));
		btnCuatro.setBackground(Color.BLACK);
		btnCuatro.setForeground(Color.WHITE);
		btnCuatro.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnCuatro.setBounds(10, 336, 75, 50);
		contentPane.add(btnCuatro);
		
		btnCinco = new JButton("5");
		btnCinco.setFont(new Font("Arial", Font.BOLD, 15));
		btnCinco.setBackground(Color.BLACK);
		btnCinco.setForeground(Color.WHITE);
		btnCinco.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnCinco.setBounds(87, 336, 75, 50);
		contentPane.add(btnCinco);
		
		btnSeis = new JButton("6");
		btnSeis.setFont(new Font("Arial", Font.BOLD, 15));
		btnSeis.setBackground(Color.BLACK);
		btnSeis.setForeground(Color.WHITE);
		btnSeis.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnSeis.setBounds(164, 336, 75, 50);
		contentPane.add(btnSeis);
		
		btnTres = new JButton("3");
		btnTres.setFont(new Font("Arial", Font.BOLD, 15));
		btnTres.setBackground(Color.BLACK);
		btnTres.setForeground(Color.WHITE);
		btnTres.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnTres.setBounds(164, 388, 75, 50);
		contentPane.add(btnTres);
		
		btnDos = new JButton("2");
		btnDos.setFont(new Font("Arial", Font.BOLD, 15));
		btnDos.setBackground(Color.BLACK);
		btnDos.setForeground(Color.WHITE);
		btnDos.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnDos.setBounds(87, 388, 75, 50);
		contentPane.add(btnDos);
		
		btnUno = new JButton("1");
		btnUno.setFont(new Font("Arial", Font.BOLD, 15));
		btnUno.setBackground(Color.BLACK);
		btnUno.setForeground(Color.WHITE);
		btnUno.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnUno.setBounds(10, 388, 75, 50);
		contentPane.add(btnUno);
		
		btnCero = new JButton("0");
		btnCero.setFont(new Font("Arial", Font.BOLD, 15));
		btnCero.setBackground(Color.BLACK);
		btnCero.setForeground(Color.WHITE);
		btnCero.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnCero.setBounds(87, 440, 75, 50);
		contentPane.add(btnCero);
		
		//--------------------------------------------------PUNTO--------------------------------------------------//
		btnPunto = new JButton(".");
		btnPunto.setFont(new Font("Arial", Font.BOLD, 15));
		btnPunto.setBackground(Color.BLACK);
		btnPunto.setForeground(Color.WHITE);
		btnPunto.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnPunto.setBounds(164, 440, 75, 50);
		contentPane.add(btnPunto);
		
		//--------------------------------------------------OPERACIONES--------------------------------------------------//
		btnNegar = new JButton("+/-");
		btnNegar.setFont(new Font("Arial", Font.BOLD, 15));
		btnNegar.setBackground(Color.BLACK);
		btnNegar.setForeground(Color.WHITE);
		btnNegar.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnNegar.setBounds(10, 440, 75, 50);
		contentPane.add(btnNegar);
		
		btnIgual = new JButton("=");
		btnIgual.setFont(new Font("Arial", Font.BOLD, 15));
		btnIgual.setBackground(new Color(43, 93, 133));
		btnIgual.setForeground(Color.WHITE);
		btnIgual.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnIgual.setBounds(241, 440, 75, 50);
		contentPane.add(btnIgual);
		
		btnSuma = new JButton("+");
		btnSuma.setFont(new Font("Arial", Font.BOLD, 15));
		btnSuma.setBackground(new Color(23, 23, 23));
		btnSuma.setForeground(Color.WHITE);
		btnSuma.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnSuma.setBounds(241, 388, 75, 50);
		contentPane.add(btnSuma);
		
		btnResta = new JButton("-");
		btnResta.setFont(new Font("Arial", Font.BOLD, 15));
		btnResta.setBackground(new Color(23, 23, 23));
		btnResta.setForeground(Color.WHITE);
		btnResta.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnResta.setBounds(241, 336, 75, 50);
		contentPane.add(btnResta);
		
		btnMultiplicacion = new JButton("x");
		btnMultiplicacion.setFont(new Font("Arial", Font.BOLD, 15));
		btnMultiplicacion.setBackground(new Color(23, 23, 23));
		btnMultiplicacion.setForeground(Color.WHITE);
		btnMultiplicacion.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnMultiplicacion.setBounds(241, 284, 75, 50);
		contentPane.add(btnMultiplicacion);
		
		btnDivision = new JButton("\u00F7");
		btnDivision.setFont(new Font("Arial", Font.BOLD, 15));
		btnDivision.setBackground(new Color(23, 23, 23));
		btnDivision.setForeground(Color.WHITE);
		btnDivision.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnDivision.setBounds(241, 232, 75, 50);
		contentPane.add(btnDivision);
		
		btnRaizCuadrada = new JButton("2\u221Ax");
		btnRaizCuadrada.setFont(new Font("Arial", Font.BOLD, 15));
		btnRaizCuadrada.setBackground(new Color(23, 23, 23));
		btnRaizCuadrada.setForeground(Color.WHITE);
		btnRaizCuadrada.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnRaizCuadrada.setBounds(164, 232, 75, 50);
		contentPane.add(btnRaizCuadrada);
		
		btnPotenciaCuadrada = new JButton("x\u00B2");
		btnPotenciaCuadrada.setFont(new Font("Arial", Font.BOLD, 15));
		btnPotenciaCuadrada.setBackground(new Color(23, 23, 23));
		btnPotenciaCuadrada.setForeground(Color.WHITE);
		btnPotenciaCuadrada.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnPotenciaCuadrada.setBounds(87, 232, 75, 50);
		contentPane.add(btnPotenciaCuadrada);
		
		btnDerivada = new JButton("1/x");
		btnDerivada.setFont(new Font("Arial", Font.BOLD, 15));
		btnDerivada.setBackground(new Color(23, 23, 23));
		btnDerivada.setForeground(Color.WHITE);
		btnDerivada.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnDerivada.setBounds(10, 232, 75, 50);
		contentPane.add(btnDerivada);
		
		btnBorrar = new JButton("<<");
		btnBorrar.setFont(new Font("Arial", Font.BOLD, 15));
		btnBorrar.setBackground(new Color(23, 23, 23));
		btnBorrar.setForeground(Color.WHITE);
		btnBorrar.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnBorrar.setBounds(241, 180, 75, 50);
		contentPane.add(btnBorrar);
		
		btnEliminarOperacion = new JButton("C");
		btnEliminarOperacion.setFont(new Font("Arial", Font.BOLD, 15));
		btnEliminarOperacion.setBackground(new Color(23, 23, 23));
		btnEliminarOperacion.setForeground(Color.WHITE);
		btnEliminarOperacion.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnEliminarOperacion.setBounds(164, 180, 75, 50);
		contentPane.add(btnEliminarOperacion);
		
		btnEliminarActual = new JButton("C");
		btnEliminarActual.setFont(new Font("Arial", Font.BOLD, 15));
		btnEliminarActual.setBackground(new Color(23, 23, 23));
		btnEliminarActual.setForeground(Color.WHITE);
		btnEliminarActual.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnEliminarActual.setBounds(87, 180, 75, 50);
		contentPane.add(btnEliminarActual);
		
		btnModulo = new JButton("%");
		btnModulo.setFont(new Font("Arial", Font.BOLD, 15));
		btnModulo.setBackground(new Color(23, 23, 23));
		btnModulo.setForeground(Color.WHITE);
		btnModulo.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnModulo.setBounds(10, 180, 75, 50);
		contentPane.add(btnModulo);
		
		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setFont(new Font("Arial", Font.BOLD, 15));
		textPane.setBounds(10, 25, 305, 125);
		textPane.setBackground(new Color(34, 34, 34));
		textPane.setForeground(Color.WHITE);
		contentPane.add(textPane);
	}

	public ControladorCalculadora getControlador() {
		return controlador;
	}

	public void setControlador(ControladorCalculadora controlador) {
		this.controlador = controlador;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JButton getBtnPunto() {
		return btnPunto;
	}

	public JButton getBtnModulo() {
		return btnModulo;
	}

	public JButton getBtnEliminarActual() {
		return btnEliminarActual;
	}

	public JButton getBtnEliminarOperacion() {
		return btnEliminarOperacion;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnDerivada() {
		return btnDerivada;
	}

	public JButton getBtnPotenciaCuadrada() {
		return btnPotenciaCuadrada;
	}

	public JButton getBtnRaizCuadrada() {
		return btnRaizCuadrada;
	}

	public JButton getBtnDivision() {
		return btnDivision;
	}

	public JButton getBtnSiete() {
		return btnSiete;
	}

	public JButton getBtnOcho() {
		return btnOcho;
	}

	public JButton getBtnNueve() {
		return btnNueve;
	}

	public JButton getBtnMultiplicacion() {
		return btnMultiplicacion;
	}

	public JButton getBtnCuatro() {
		return btnCuatro;
	}

	public JButton getBtnCinco() {
		return btnCinco;
	}

	public JButton getBtnSeis() {
		return btnSeis;
	}

	public JButton getBtnResta() {
		return btnResta;
	}

	public JButton getBtnUno() {
		return btnUno;
	}

	public JButton getBtnDos() {
		return btnDos;
	}

	public JButton getBtnTres() {
		return btnTres;
	}

	public JButton getBtnSuma() {
		return btnSuma;
	}

	public JButton getBtnNegar() {
		return btnNegar;
	}

	public JButton getBtnCero() {
		return btnCero;
	}

	public JButton getBtnIgual() {
		return btnIgual;
	}
	
	
}
