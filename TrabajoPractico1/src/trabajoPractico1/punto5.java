package trabajoPractico1;

import java.util.Scanner;

public class punto5 {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in); // objeto donde se guardaran los datos solicitados

		int randNum1, randNum2 = 0;
		int contGanadas = 0,contPerdidas = 0;
		int seleccion, seguirJugando;

		do {
			do {
				randNum1 = (int) (Math.random() * 10); // Genero un numero aleatorio entre 0..10
				randNum2 = (int) (Math.random() * 10); // Genero un numero aleatorio entre 0..10
			} while (randNum1 != randNum2);
			
			System.out.print("Seleccione valor 1 o 2: ");
			seleccion = entrada.nextInt();

			switch (seleccion) {
			case 1:
				if (randNum1 > randNum2) {
					System.out.println("Gano!!!");
					contGanadas ++;
				} else {
					System.out.println("Perdio!!!");
					contPerdidas ++;
				}
				break;
			case 2:
				if (randNum2 > randNum1) {
					System.out.println("Gano!!!");
					contGanadas ++;
				} else {
					System.out.println("Perdio!!!");
					contPerdidas ++;
				}
				break;
			default:
				System.out.println("Opcion invalida");
				break;
			}
			
			System.out.print("Desea seguir jugando? 1)si 2)no: ");
			seguirJugando = entrada.nextInt();

		}while(seguirJugando!=2);
		
		System.out.println("Cantidad Partidas Ganadas: " + contGanadas);
		System.out.println("Cantidad Partidas Perdidas: " + contPerdidas);

	}

}
