/* Funciones de java.util.Scanner
 * 
 * var.nextInt() : lee un entero
 * var.nextFloat() : lee un flotante
 * var.nextDouble() : lee un Double
 * var.next() : lee un string, pero hasta que encuentra un espacio
 * var.nextLive() : lee un string, sin importar que haya espacios
 * var.next().charAt(0) : lee el primer caracter de un string
 * 
 */

package trabajoPractico1;

import java.util.Scanner;

public class punto2 {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in); //objeto donde se guardaran los datos solicitados
		
		String frase1;
		String frase2;
		
		System.out.print("Ingrese la 1er frase: ");frase1 = entrada.nextLine();
		System.out.print("Ingrese la 2da frase:");frase2 = entrada.nextLine();
		
		System.out.println(frase1 + " " + frase2);
		
	}

}
