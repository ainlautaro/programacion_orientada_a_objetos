package trabajoPractico1;

import java.util.Scanner;

public class punto4 {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in); //objeto donde se guardaran los datos solicitados

		String frase1,frase2;
		boolean iguales;
		
		System.out.print("Ingrese la 1er Frase: ");frase1 = entrada.nextLine();
		System.out.print("Ingrese la 2da Frase: ");frase2 = entrada.nextLine();
		
		iguales = frase1 == frase2;
		System.out.println("Comparando frases con if: " + iguales);
		
		iguales = frase1.equals(frase2);
		System.out.println("Comparando frases con .equals(): " + iguales);
	}

}
