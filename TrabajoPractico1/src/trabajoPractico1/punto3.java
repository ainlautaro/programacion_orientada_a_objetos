package trabajoPractico1;

import java.util.Scanner;

public class punto3 {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in); //objeto donde se guardaran los datos solicitados
		int num, aleatorio = 0;
		
		aleatorio = (int) (Math.random()*10); //Genero un numero aleatorio entre 0..10
		
		System.out.print("Ingrese un Numero: ");num = entrada.nextInt();
		
		if (num == aleatorio) {
			System.out.println(num + " = " + aleatorio);
		}else {
			System.out.println(num + " != " + aleatorio);
		}
		
		if (num >= aleatorio) {
			System.out.println(num + " >= " + aleatorio);
		}else {
			System.out.println(num + " <= " + aleatorio);
		}
		
		if (num > aleatorio) {
			System.out.println(num + " > " + aleatorio);
		}else {
			System.out.println(num + " < " + aleatorio);
		}
		
	}

}
