package modelo;

import java.util.ArrayList;

public class Agenda {

	private ArrayList<Persona> agenda = new ArrayList<Persona>();
	
	public Boolean agregar(Persona persona) {
		return getAgenda().add(persona);
	}
	
	@Override
	public String toString() {
		String cadena = "Nombre\t\tApellido\tTel�fono\n";
		
		for (Persona persona : agenda) {
			cadena = cadena + persona.getNombre()   + "\t\t" + 
							  persona.getApellido() + "\t\t" + 
							  persona.getTelefono() + "\n";
		}
		
		return cadena;
	}

	public ArrayList<Persona> getAgenda() {
		return agenda;
	}

	public void setAgenda(ArrayList<Persona> agenda) {
		this.agenda = agenda;
	}
	
	
}
