package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorAgenda;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaNuevoUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldApellido;
	private JTextField textFieldNombre;
	private JTextField textFieldTelefono;
	private ControladorAgenda controlador; 
	private JButton btnAceptar;
	private JButton btnCancelar;


	/**
	 * Create the frame.
	 */
	public VistaNuevoUsuario(ControladorAgenda controlador) {
		this.setControlador(controlador);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 370, 420);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblNuevoUsuario = new JLabel("   Nuevo Usuario");
		lblNuevoUsuario.setFont(new Font("Tahoma", Font.BOLD, 14));
		contentPane.add(lblNuevoUsuario, BorderLayout.NORTH);
		
		JPanel panelCancelarAceptar = new JPanel();
		panelCancelarAceptar.setBackground(Color.WHITE);
		FlowLayout fl_panelCancelarAceptar = (FlowLayout) panelCancelarAceptar.getLayout();
		fl_panelCancelarAceptar.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panelCancelarAceptar, BorderLayout.SOUTH);
		
		btnCancelar = new JButton("Cancelar");
		panelCancelarAceptar.add(btnCancelar);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControlador());
		panelCancelarAceptar.add(btnAceptar);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		contentPane.add(rigidArea, BorderLayout.WEST);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		contentPane.add(rigidArea_1, BorderLayout.EAST);
		
		JPanel panelDatosIngreso = new JPanel();
		panelDatosIngreso.setBackground(Color.WHITE);
		contentPane.add(panelDatosIngreso, BorderLayout.CENTER);
		panelDatosIngreso.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNombre.setBounds(10, 34, 180, 14);
		panelDatosIngreso.add(lblNombre);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(10, 59, 284, 25);
		panelDatosIngreso.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblApellido.setBounds(10, 95, 180, 14);
		panelDatosIngreso.add(lblApellido);
		
		textFieldApellido = new JTextField();
		lblApellido.setLabelFor(textFieldApellido);
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(10, 120, 284, 25);
		panelDatosIngreso.add(textFieldApellido);
		
		JLabel lblTelefono = new JLabel("Tel\u00E9fono");
		lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTelefono.setBounds(10, 156, 180, 14);
		panelDatosIngreso.add(lblTelefono);
		
		textFieldTelefono = new JTextField();
		textFieldTelefono.setColumns(10);
		textFieldTelefono.setBounds(10, 181, 284, 25);
		panelDatosIngreso.add(textFieldTelefono);
		
	}
	
	public ControladorAgenda getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAgenda controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JTextField getTextFieldApellido() {
		return textFieldApellido;
	}

	public JTextField getTextFieldNombre() {
		return textFieldNombre;
	}

	public JTextField getTextFieldTelefono() {
		return textFieldTelefono;
	}
	
}
