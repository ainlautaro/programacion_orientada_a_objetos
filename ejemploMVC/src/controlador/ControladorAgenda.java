//Utilizamos implementes para interfaces

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Agenda;
import modelo.Persona;
import vista.VistaNuevoUsuario;

public class ControladorAgenda implements ActionListener{

	private VistaNuevoUsuario vista;
	private Agenda Agenda;
	
	public ControladorAgenda() {
		super();
		this.vista = new VistaNuevoUsuario(this);
		this.Agenda = new Agenda();
		
		this.vista.setVisible(true);//hacemos visible la vista
	}
	
	@Override										//seguir
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(getVista().getBtnAceptar())) { //verificamos que btn desencadeno el evento
			String nombre = getVista().getTextFieldNombre().getText();
			String apellido = getVista().getTextFieldApellido().getText();
			String telefono = getVista().getTextFieldTelefono().getText();
			
			getAgenda().agregar(new Persona(nombre, apellido, telefono));
			System.out.println(Agenda);
			
		}
		
	}

	public VistaNuevoUsuario getVista() {
		return vista;
	}

	public void setVista(VistaNuevoUsuario vista) {
		this.vista = vista;
	}

	public Agenda getAgenda() {
		return Agenda;
	}

	public void setAgenda(Agenda agenda) {
		Agenda = agenda;
	}
	
}
