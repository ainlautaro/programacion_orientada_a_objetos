package controlador;

import java.awt.EventQueue;

import vista.VistaNuevoUsuario;

public class Main {

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorAgenda();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
