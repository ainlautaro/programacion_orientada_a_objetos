import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class EjemploLectura {

	public static void main(String[] args) {
		
		try(Scanner input = new Scanner(Paths.get("assets/clientes.txt"))){
			System.out.printf("%-10s %-12fs %-12s %10s %n", "cuenta", "nombre", "apellido", "balance");
			
			while (input.hasNext()) {
				System.out.printf("%-10d %-12s %-12s %10.2f %n", input.nextInt(), input.next(), input.next(), input.nextDouble());
			}
																		
		}catch(IOException e){
			e.printStackTrace();
		}
				

	}

}
