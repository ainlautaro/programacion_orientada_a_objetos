import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class EjemploCreacion {

	public static void main(String[] args) {
		
		try(Formatter output = new Formatter("assets/clientes.txt")){
			Scanner input = new Scanner(System.in);
			System.out.printf("%s%n%s",
					"Ingrese numero de cuenta, nombre, apellido, balance.",
					"Ingrese ctrl+z para finalizar.");
			
			while (input.hasNext()) {
				try {
					output.format("%d %s %s %.2f%n", input.nextInt(), input.next(), input.next(), input.nextDouble());
				}catch(NoSuchElementException elemntException){
					System.err.println("Valor invalido, ingrese otra vez");
					input.nextLine();
				}
				System.out.println(": ");
			}
			
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
				

	}

}
