import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class EjemploPath {

	public static void main(String[] args) throws IOException{
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Ingrese el nombre del archivo o el directorio: ");
		Path path = Paths.get(input.nextLine());
		
		//verifico si el path ingresado existe, ya sea un archivo o directorio
		if (Files.exists(path)) {
			
			System.out.printf("%n%s existe%n", path.getFileName());										//imprimo el nombre seguido de "existe"
			System.out.printf("%s es un directorio%n", Files.isDirectory(path)? "es":"no es");			//indidico si es o no un directorio con un if corto
			System.out.printf("%s es una direccion absoluta%n", path.isAbsolute()? "es":"no es");		//indidico si es o no un directorio absoluto con un if corto
			System.out.printf("ultima modificacion: %s%n", Files.getLastModifiedTime(path));			//muestro la ultima modificacion con el siguiente formato: 2020-09-02T14:27:31.91972Z
			System.out.printf("tamanio %s%n", Files.size(path));										//imprimo el tamanio del archivo
			System.out.printf("direccion: %s%n", Files.size(path));										//imprimo el tamanio del directorio
			System.out.printf("direccion abosuluta: %s%n", path.toAbsolutePath());						//muestro la direccion absoluta
		
			//verifico si el path ingresado es un directorio
			if (Files.isDirectory(path)) {
				
				System.out.printf("%ncontenido:%n");
				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path); //abra el archivo y lo coloca como un string
				
				//recorro el directorio
				for (Path p : directoryStream) {
					System.out.println(p);			//imprime uno a uno los archivos si es que el directorio contiene
				}
			}else {
				System.out.printf("%s no existe%n", path);
			}
		}

	}

}
