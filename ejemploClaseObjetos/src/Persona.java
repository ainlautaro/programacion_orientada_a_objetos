import java.time.LocalDate;
import java.util.Random;

public class Persona {

	//atributos de la clase
	private String nombre, genero, email, password;
	private LocalDate fecha_nacimiento;
	
	//constructor
	public Persona(String nombre, String genero, String email, LocalDate fecha_nacimiento) {
		super();
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFecha_nacimiento(fecha_nacimiento);
		
		this.setPassword(this.autogenerar_password());
	}
	
	//constructor sobrecargado
	public Persona(String nombre, String genero, String email, String password, LocalDate fecha_nacimiento) {
		super();
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFecha_nacimiento(fecha_nacimiento);
		
		this.setPassword(password);
	}

	//getters y setters
	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public final String getGenero() {
		return genero;
	}

	public final void setGenero(String genero) {
		this.genero = genero;
	}

	public final String getEmail() {
		return email;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final String getPassword() {
		return password;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public final void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	//metodos
	@Override
	public String toString() {
		return "Nombre: " + nombre + "\nSexo: " + genero + "\nEmail= " + email + "\nPassword= " + password
				+ ", fecha_nacimiento=" + fecha_nacimiento;
	}
	
	//Metodo para imprimir
	public void imprimir() {
		System.out.println(this.toString());
	}
	
	//metodo para generar una password
	private String autogenerar_password() {
		Random rnd = new Random();
		Integer num = rnd.nextInt(100)+1;
		return "pas"+ num;
	}
	
}
