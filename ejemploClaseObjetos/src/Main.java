import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		
		//creamos el objeto persona
		Persona persona = new Persona("Ain", "masculino", "ainlautaro7@gmail.com", LocalDate.of(2001, 1, 23));
		
		Persona persona2 = new Persona("Lautaro", "masculino", "lautaro@gmail.com", "pass123", LocalDate.of(2001, 05, 5));
		
		//llamos a el metodo
		System.out.println("Persona 1:");
		persona.imprimir();
		
		System.out.println("\nPersona 2:");
		persona2.imprimir();

	}

}
