
public class Main {

	public static void main(String[] args) {
		
		EmpleadoPorComision empleado = new EmpleadoPorComision("Ain", "Garcia", "40205760", 300000.0, 0.1);
		EmpleadoBaseMasComision empleado2 = new EmpleadoBaseMasComision("Sebastian", "Cereminati", "37878505", 300000.0, 0.1, 50000.0);
		
		System.out.println(empleado);
		System.out.println(empleado2);
		
	}

}
