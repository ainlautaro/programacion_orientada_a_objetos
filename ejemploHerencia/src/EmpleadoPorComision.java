public class EmpleadoPorComision {

	private String nombre, apellido, dni;
	private Double ventasBrutas, tarifaComision;
	
	public EmpleadoPorComision(String nombre, String apellido, String dni, Double ventasBrutas, Double tarifaComision) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.ventasBrutas = ventasBrutas;
		this.tarifaComision = tarifaComision;
	}
	
	public Double ingresos(){
		return getVentasBrutas() * getTarifaComision();
	}

	@Override
	public String toString() {
		return  "\nNombre: " + getNombre()+
				"\nApellido: " + getApellido()+
				"\nDNI: " + getDni()+
				"\nVentas Brutas: " + getVentasBrutas()+
				"\nTarifa Comision: " + getTarifaComision()+
				"\nIngresos Totales: " + ingresos();
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public final String getApellido() {
		return apellido;
	}

	public final void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public final String getDni() {
		return dni;
	}

	public final void setDni(String dni) {
		this.dni = dni;
	}

	public final Double getVentasBrutas() {
		return ventasBrutas;
	}

	public final void setVentasBrutas(Double ventasBrutas) {
		this.ventasBrutas = ventasBrutas;
	}

	public final Double getTarifaComision() {
		return tarifaComision;
	}

	public final void setTarifaComision(Double tarifaComision) {
		this.tarifaComision = tarifaComision;
	}

}
