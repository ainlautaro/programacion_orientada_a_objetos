
public class EmpleadoBaseMasComision extends EmpleadoPorComision {

	private Double salarioBase;

	public EmpleadoBaseMasComision(String nombre, String apellido, String dni, Double ventasBrutas,
			Double tarifaComision, Double salarioBase) {
		super(nombre, apellido, dni, ventasBrutas, tarifaComision);

		this.salarioBase = salarioBase;
	}

	public Double ingresos() {
		return getSalarioBase() + super.ingresos();
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nSalario Base: " + getSalarioBase();
	}
	
	public Double getSalarioBase() {
		return salarioBase;
	}

}
