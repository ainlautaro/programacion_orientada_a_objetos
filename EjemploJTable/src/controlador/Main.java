package controlador;

import java.awt.EventQueue;

import vista.VistaTablas;

public class Main {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Controlador();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
