package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.RowFilter;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.Mascota;
import vista.VistaTablas;

public class Controlador implements ListSelectionListener, ActionListener, KeyListener{
	private VistaTablas vista;
	
	public Controlador() {
		super();
		this.setVista(new VistaTablas(this));
		Mascota mascota;
		
		for (int i = 0; i < 4; i++) {
			mascota = new Mascota((i+1), "mascota" + (i+1), "especie" + (i+1), new Random().nextInt(10)+1);	//creamos un objeto mascota
			
			Object[] row = {String.valueOf(mascota.getId()),mascota.getNombre(),mascota.getEspecie(), String.valueOf(mascota.getEdad())};
			
			this.getVista().getModeloTabla().addRow(row);	//insertamos los datos a la tabla
		}
	
		this.getVista().setVisible(true);
	}

	//metodo del ListSelectionListener (escuchador de la seleccion en la tabla
	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		this.getVista().getBtnEjemplo().setEnabled(true);	//habilito el boton al seleccionar una fila
	}
	
	//Metodo del ActionListener
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(this.getVista().getBtnEjemplo())) {
			
			//obtengo la fila seleccionada
			//this.getVista().getTable().getSelectedRow();	
			
			//obtengo la columna seleccionada
			//this.getVista().getTable().getSelectedColumn(); 	
			
			//Imprimimos el dato seleccionado
			//System.out.println(this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), this.getVista().getTable().getSelectedColumn()));
			
			//Modificamos el dato seleccionado
			//this.getVista().getModeloTabla().setValueAt("Leon", this.getVista().getTable().getSelectedRow(), this.getVista().getTable().getSelectedColumn());
		
			//Eliminamos una fila seleccionada
			this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());
		}
	}
	
	//metodo
	public void search() {
		TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(this.getVista().getModeloTabla());	//TableRowSorter<DefaultTableModel> es un array que contiene los elementos de la fila
		this.getVista().getTable().setRowSorter(tr);
		tr.setRowFilter(RowFilter.regexFilter(this.getVista().getTextFieldBuscar().getText()));
	}
	
	//Metodos del KeyListener	
	@Override
	public void keyPressed(KeyEvent e) {
		this.search();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.search();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public VistaTablas getVista() {
		return vista;
	}

	public void setVista(VistaTablas vista) {
		this.vista = vista;
	}
	
}
