package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.Controlador;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;

public class VistaTablas extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	//CONSULTAR POR ESTA LINEA
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnEliminar;
	private DefaultTableModel modeloTabla;	//creamos un modelo por defecto de la tabla
	private Controlador controlador;
	private JTextField textFieldBuscar;

	/**
	 * Create the frame.
	 */
	public VistaTablas(Controlador controlador) {
		this.setControlador(controlador);
		
		setTitle("Ejemplo JTable");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//-----------------------LBL MASCOTAS-----------------------//
		JLabel lblMascotas = new JLabel("Mascotas");
		lblMascotas.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblMascotas.setHorizontalAlignment(SwingConstants.CENTER);
		lblMascotas.setBounds(10, 0, 409, 34);
		contentPane.add(lblMascotas);
		
		//-----------------------SECCION BUSCAR-----------------------//
		//-------------LBL BUSCAR
		JLabel lblBuscar = new JLabel("Buscar:");
		lblBuscar.setBounds(10, 33, 45, 23);
		contentPane.add(lblBuscar);
		
		//-------------TXTFIELD BUSCAR
		textFieldBuscar = new JTextField();
		lblBuscar.setLabelFor(textFieldBuscar);
		textFieldBuscar.setBounds(62, 33, 248, 23);
		contentPane.add(textFieldBuscar);
		textFieldBuscar.setColumns(10);
		textFieldBuscar.addKeyListener(this.getControlador());
		
		//-----------------------PANEL DONDE ESTA LA TABLA-----------------------//
		JPanel panelTabla = new JPanel();
		panelTabla.setBackground(Color.WHITE);
		panelTabla.setBounds(10, 67, 300, 183);
		contentPane.add(panelTabla);
		panelTabla.setLayout(null);
		
		//-----------------------TABLA-----------------------//
		String header[] = {"ID","Nombre","Especie","Edad"};			//encabezado de la tabla
		
		modeloTabla = new DefaultTableModel(null, header);	
		//seteamos el modelo con el encabezado, el null indica que esta estara vacia para luego ser llenada
		
		table = new JTable(modeloTabla);
		table.setBackground(Color.WHITE);
		table.setBounds(0, 0, 300, 183);
		table.setVisible(true);
		table.getSelectionModel().addListSelectionListener(getControlador());
		
		//detalles de la tabla
		table.setDefaultEditor(Object.class, null);					//evitamos que los datos de la tabla se puedan editar (desde la tabla)
		//ocultamos la columna del id
		table.getColumnModel().getColumn(0).setMaxWidth(0);			//seteamos un ancho maximo en 0
		table.getColumnModel().getColumn(0).setMinWidth(0);			//seteamos un ancho minimo en 0
		table.getColumnModel().getColumn(0).setPreferredWidth(0);
		//deshabilitamos la seleccion multiple de celdas
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		//-----------------------SCROLLPANE DE LA TABLA-----------------------//
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 300, 183);
		scrollPane.setViewportView(table);			//al scrollpanel le insertamos la tabla
		panelTabla.add(scrollPane);
		
		//-----------------------BOTONES-----------------------//
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(330, 65, 89, 23);
		btnEliminar.addActionListener(this.getControlador());
		contentPane.add(btnEliminar);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnEjemplo() {
		return btnEliminar;
	}

	public void setBtnEjemplo(JButton btnEjemplo) {
		this.btnEliminar = btnEjemplo;
	}
	
	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}

	public Controlador getControlador() {
		return controlador;
	}

	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}
	
	public JTextField getTextFieldBuscar() {
		return textFieldBuscar;
	}

	public void setTextFieldBuscar(JTextField textFieldBuscar) {
		this.textFieldBuscar = textFieldBuscar;
	}
}
