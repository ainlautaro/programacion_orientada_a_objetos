
public class Cliente {

	private String nombre, apellido, telefono;
	private Vehiculo vehiculo;
	
	public Cliente(String nombre, String apellido, String telefono, Vehiculo vehiculo) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.vehiculo = vehiculo;
	}
	
	public final String getNombre() {
		return nombre;
	}
	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public final String getApellido() {
		return apellido;
	}
	public final void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public final String getTelefono() {
		return telefono;
	}
	public final void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public final Vehiculo getVehiculo() {
		return vehiculo;
	}
	public final void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}
	
	
	
}
