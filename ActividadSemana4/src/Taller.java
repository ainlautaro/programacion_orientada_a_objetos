import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Taller {

	private String nombreTaller, direccion, telefono;
	private Integer precioXhora = 70;

	private ArrayList<Vehiculo> vehiculos = new ArrayList<>();

	public Taller(String nombreTaller, String direccion, String telefono) {
		
		this.nombreTaller = nombreTaller;
		this.direccion = direccion;
		this.telefono = telefono;
	}
	
	public void ingresarDatos() {
		String patente = "", modelo= "", averia= "", tipoVehiculo= "";
		Integer horasReparacion = -1;
		ArrayList<Vehiculo> backup = new ArrayList<>();
		
		Path csv = Paths.get("assets/RegistroVehiculos.csv");
		 
		//verifico si el archivo existe para bajar los datos y no perderlos
			if (Files.exists(csv)) {
				
				System.out.println("existe");
				
				try(Scanner input = new Scanner(Paths.get("assets/RegistroVehiculos.csv"))){
					
					input.useDelimiter("\\r\\n|\\n\\r");
					String[] datos;
					
					while (input.hasNext()) {
						
						datos = input.next().split(";");
						patente = datos[0];
						modelo = datos[1];
						averia = datos[2];
						tipoVehiculo = datos[3];
						horasReparacion = Integer.valueOf(datos[4]);
						
						switch (tipoVehiculo.toUpperCase()) {
						case "AUTO":
							Auto auto = new Auto(datos[0], datos[1], datos[2], datos[3], 0.5, Integer.valueOf(datos[4]));
							backup.add(auto);
							break;
						case "CAMIONETA":
							Camioneta camioneta = new Camioneta(datos[0], datos[1], datos[2], datos[3], 0.5, Integer.valueOf(datos[4]));
							backup.add(camioneta);
							break;
						case "CAMION":
							Camion camion = new Camion(datos[0], datos[1], datos[2], datos[3], 0.5, Integer.valueOf(datos[4]));
							backup.add(camion);
							break;
						default:
							break;
						}
						
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				crearCSV(backup, patente, modelo, averia, tipoVehiculo, horasReparacion);

				
			}
			
			else {

				crearCSV(backup, patente, modelo, averia, tipoVehiculo, horasReparacion);
				
		}
			
		}
	
	//Metodo en el que creamos el csv para reutilizar codigo
	protected void crearCSV(ArrayList<Vehiculo> backup,String patente, String modelo, String averia, String tipoVehiculo, Integer horasReparacion){
		Scanner input = new Scanner(System.in);
		System.out.println("\nDatos vehiculo ");
		System.out.print("Ingrese la patente: ");
		patente = input.nextLine();
		
		System.out.print("Ingrese modelo: ");
		modelo = input.nextLine();
		
		System.out.print("Ingrese averia: ");
		averia = input.nextLine();
		
		System.out.print("Ingrese la cantidad de horas estimada de reparacion: ");
		horasReparacion = Integer.valueOf(input.nextLine());
		
		System.out.print("Ingrese el tipo de vehiculo: ");
		tipoVehiculo = input.nextLine();
		
		try(Formatter output =  new Formatter("assets/RegistroVehiculos.csv")){
			try {
				switch (tipoVehiculo.toUpperCase()) {
				
					case "AUTO":
						Vehiculo auto = new Auto(patente, modelo, averia, "auto" ,0.5, horasReparacion);
						this.getVehiculos().add(auto);
					
						output.format("%s;%s;%s;%s;%d%n",auto.getPatente(), auto.getModelo(), 
								auto.getAveria(), auto.getTipoVehiculo(), auto.getHorasReparacion());
					break;
					
					case "CAMIONETA":
						Vehiculo camioneta = new Camioneta(patente, modelo, averia, "camioneta", 0.10, horasReparacion);
						this.getVehiculos().add(camioneta);
					
						output.format("%s;%s;%s;%s;%d%n",camioneta.getPatente(), camioneta.getModelo(), 
							camioneta.getAveria(), camioneta.getTipoVehiculo(), camioneta.getHorasReparacion());
					break;
					
					case "CAMION":
						Vehiculo camion = new Camion(patente, modelo, averia, "camion", 0.15, horasReparacion);
						this.getVehiculos().add(camion);
					
						output.format("%s;%s;%s;%s;%d%n",camion.getPatente(), camion.getModelo(), 
							camion.getAveria(), camion.getTipoVehiculo(), camion.getHorasReparacion());
					break;
					
					default:
					break;
				}
				
				//una vez ingresamos el nuevo objeto al archivo, subimos el backup
				for (int i = 0; i < backup.size(); i++) {
					output.format("%s;%s;%s;%s;%d%n",
							backup.get(i).getPatente(), 
							backup.get(i).getModelo(),
							backup.get(i).getAveria(),
							backup.get(i).getTipoVehiculo(),
							backup.get(i).getHorasReparacion());
				}
				
		}catch(NoSuchElementException elemntException){
		}
			
	} catch(FileNotFoundException e) {
		e.printStackTrace();
	}
	}
	
	
	//Metodo para obtener el costo de la reparacion segun el producto a reparar
	public Double ingresarCosto(Vehiculo vehiculo) {
		Double precio = 0.0;
		
		switch (vehiculo.getAveria().toUpperCase()) {
		case "BOMBA DE AGUA":
			precio = ((this.getPrecioXhora()*vehiculo.getHorasReparacion())+1000.00);
			precio += precio*vehiculo.getRecargo();
			break;
		case "FRENOS":
			precio = ((this.getPrecioXhora()*vehiculo.getHorasReparacion())+1000.00);
			precio += precio*vehiculo.getRecargo();
			break;
		case "EMBRAGUE":
			precio = ((this.getPrecioXhora()*vehiculo.getHorasReparacion())+1000.00);
			precio += precio*vehiculo.getRecargo();
			break;			
		default:
			break;
		}
		
		return precio;
	}

	
	//----------------------------Getter y Setters----------------------------//
	protected final String getNombreTaller() {
		return nombreTaller;
	}
	protected final void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}


	protected final String getDireccion() {
		return direccion;
	}
	protected final void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	protected final String getTelefono() {
		return telefono;
	}
	protected final void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public final Integer getPrecioXhora() {
		return precioXhora;
	}
	public final void setPrecioXhora(Integer precioXhora) {
		this.precioXhora = precioXhora;
	}


	public final ArrayList<Vehiculo> getVehiculos() {
		return vehiculos;
	}
	public final void setVehiculos(ArrayList<Vehiculo> vehiculo) {
		this.vehiculos = vehiculo;
	}


	
}