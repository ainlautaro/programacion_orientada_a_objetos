
public class Vehiculo {

	private String patente, modelo, averia, tipoVehiculo;
	private Integer horasReparacion;
	private Double recargo;
	
	//Constructor Vehiculo
	public Vehiculo(String patente, String modelo, String averia, String tipoVehiculo, Integer horasReparacion) {
		
		super();
		this.patente = patente;
		this.modelo = modelo;
		this.averia = averia;
		this.tipoVehiculo = tipoVehiculo;
		this.horasReparacion = horasReparacion;
	}

	//Getters y Setters
	public final String getPatente() {
		return patente;
	}
	public final void setPatente(String patente) {
		this.patente = patente;
	}

	public final String getModelo() {
		return modelo;
	}
	public final void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public final String getAveria() {
		return averia;
	}
	public final void setAveria(String averia) {
		this.averia = averia;
	}

	public final Double getRecargo() {
		return recargo;
	}
	public final void setRecargo(Double recargo) {
		this.recargo = recargo;
	}

	public final Integer getHorasReparacion() {
		return horasReparacion;
	}
	public final void setHorasReparacion(Integer horasReparacion) {
		this.horasReparacion = horasReparacion;
	}

	public final String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public final void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	
	
	
	
}
