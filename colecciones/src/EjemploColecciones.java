import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingDeque;

import com.sun.glass.ui.Size;

public class EjemploColecciones {
	
	public static void main(String[] args) {
		
		ArrayList<String> colores = new ArrayList<String>();	//instancion un arraylist
		
		//a�ado elementos al arrayList con la clase *add*;
		colores.add("Azul");
		colores.add("Verde");
		colores.add("Amarillo");
		colores.add("Naranja");
		
		//recorro el arrayList con un foreach
		for (String unColor : colores) {
			System.out.println(unColor);
		}
		
		//se remueve un elemento con la clase *remove*
		colores.remove("Azul");
		
		//tambien se puede imprimir un arraylist con un sout
		System.out.println("\nPrimera lista de colores:");
		System.out.println(colores);
		
		//tambien se pueden ingresar elementos de manera *estatica*
		String[] colores2 = {"Negro", "azul", "Violeta", "Rosado"};
		
		//Se pueden unir dos listas con la clase *addAll*
		System.out.println("\nPrimera lista de colores junto con la Segunda lista:");
		colores.addAll(Arrays.asList(colores2));			//para castear una lista estatica se usa la clase *Arrays.asList(listaEstatica)*
		System.out.println(colores);
		
		//elementos de la lista convertidos a MAYUSCULA
		System.out.println("\nElementos de la lista convertidos a MAYUSCULA:");
		toUpperCase(colores);
		System.out.println(colores);
		
		//Elimino una cantidad determinada de elementos de la lista
		System.out.println("\nCreamos una sublista recortando la lista previa:");
		colores.subList(0, 3).clear();
		System.out.println(colores);
		
		//Iterar hacia atras con un iterador
		System.out.println("\nRecorremos la lista hacia atras:");
		ListIterator<String> iterator = colores.listIterator(colores.size());
		while (iterator.hasPrevious()) {
			System.out.print(iterator.previous()+" ");
		}
		
		
		//-------------------METODOS DE LA CLASE *COLLECTION*-------------------//
		
		//Metodo *copy*, para utilizar este metodo primero debemos declarar otra lista y 
		//luego asigarle el suficiente espacio, para que la lista que se quiere copiar entre
		System.out.println("\n\nLista copiada:");
		ArrayList<String> colores3 = new ArrayList<String>();
		String[] i = {"","","",""};
		colores3.addAll(Arrays.asList(i));
	    Collections.copy(colores3, colores);
	    System.out.println(colores3);
	    
	    //Metodo *fill*, llena la lista con el objeto que le especifiquemos
	    System.out.println("\nLista llena con un objeto espec�fico:");
	    Collections.fill(colores3, "Azul");
	    System.out.println(colores3);
	    
	    //Metodo *sort*, este metodo ordena los elementos de una lista
	    System.out.println("\nObjetos ordenados por el metodo *sort*:");
	    Collections.sort(colores);
	    System.out.println(colores);
		
	    //Metodo *shuffle* deseordena los elementos de la lista, de manera aleatoria
	    System.out.println("\nElementos desordenados por el metodo *shuffle*:");
	    Collections.shuffle(colores);
	    System.out.println(colores);
	    
	    //Metodo *binarySearch*, busqueda binaria de un objeto, devuelve el indice
	    System.out.println("\nBusqueda binaria de un elemento:");
	    System.out.println(colores);
	    System.out.println("Esta en la posicion: " + Collections.binarySearch(colores, "ROSADO"));
	    
	    //Metodo *frequency*, retorna la cantidad de veces que un elemento se encuentra en la lista
	    System.out.println("\nCantidad de veces que 'Azul' se encuentra en la lista:");
	    System.out.println(colores);
	    System.out.println(Collections.frequency(colores, "AZUL"));
	    System.out.println(colores3);
	    System.out.println(Collections.frequency(colores3, "Azul"));
	    
	    //Metodo *disjoint*, retorna true si 2 listas tienen
	    System.out.println("\nCompruebo si dos listan, tienen elementos en comun");
	    System.out.println("lista1 tiene elementos en comun con lista2? " + Collections.disjoint(colores, colores3));
	    
	    //LinkedList - Listas enlazadas
	    System.out.println("\nAgregamos dos elementos a la lista, uno al inicio y otro al final");
	    LinkedList<String> llcolores = new LinkedList<String>(colores);
	    llcolores.addLast("verde");
	    llcolores.addFirst("rojo");
	    System.out.println(llcolores);
	    
	    llcolores.element();			//muestra pero no retira el elemento de la cabecera
	    llcolores.poll();				//retira la cabecera
	    llcolores.push("Violeta");		//inserta un elemento por el frente de la lista
	    llcolores.pop();				//retira el primer elemento de la lista
	    
	    //PriorityQueue, cola con orden de prioridad\
	    System.out.println("\nCola con orden de prioridad:");
	    PriorityQueue<String> queue = new PriorityQueue<String>(colores);
	    System.out.println(queue);
	    
	    queue.offer("MAGENTA");		//Agregamos un elemento a la cola de prioridad
	    System.out.println(queue);
	    
	    //HashSet, colecciones que guardan elementos unicos en una tabla
	    System.out.println("\nColeccion de datos unicos:");
	    HashSet<String> set = new HashSet<String>(colores);
	    set.add("NARANJA");
	    set.add("NARANJA");		//No se pueden agregar elementos duplicados
	    System.out.println(set);
	    
	    //TreeSet, guarda elementos de forma ordenada en un arbol
	    System.out.println("\nArbol con elementos ordenados:");
	    TreeSet<String> tree = new TreeSet<String>(colores);
	    tree.add("CYAN");
	    tree.add("TURQUEZA");
	    System.out.println(tree);
	    
	    //HashMap, las estructuras tipo *map*, asocian key's con valores, donde cada "key" es unica
	    System.out.println("\nHashMap:");
	    HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
	    Scanner scanner = new Scanner(System.in);
	    
	    System.out.print("Ingrese una oracion: ");
	    String input = scanner.nextLine();
	    String[] tokens = input.split(" ");
	    
	    for (String token : tokens) {
			String palabra = token.toLowerCase();
			if (hashMap.containsKey(palabra)) {
				int count = hashMap.get(palabra);
				hashMap.put(palabra, count+1);
			}else {
				hashMap.put(palabra, 1);
			}
		}
	    
	    System.out.println(hashMap);
	    
	}
	 
	//creamos un metodos estatico para convertir cada elemento a mayusculas
	private static void toUpperCase(List<String> lista) {
		//creo un iterador auxiliar al cual le asigno la lista que viene como parametro
		ListIterator<String> iterator = lista.listIterator();
		
		//con un *hasNext()* verifico que la lista todavia tenga elementos
		while (iterator.hasNext()) {
			String color = iterator.next();			//con *.next()*, asigno el elemento de la lista
			iterator.set(color.toUpperCase());		//con *.set()*, agrego los elementos a la lista
			
		}
	}
	
//	private static void toLowerCase(List<String> lista) {
//		ListIterator<String> iterator = lista.listIterator();
//		while (iterator.hasNext()) {
//			String color = iterator.next();
//			iterator.set(color.toLowerCase());
//			
//		}
//	}
	
}
