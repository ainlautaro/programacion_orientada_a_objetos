import java.security.SecureRandom;
import java.util.Random;

public class EjemplosBasicos {

	public static void main(String[] args) {
		
		//-----------------------------------------------------FUNCIONES DEL MATH-----------------------------------------------------//
		System.out.println("5 elevado a 2 = " + Math.pow(5, 2));
		System.out.println("Raiz 2 de 900 = " + Math.sqrt(900));
		System.out.println("El valor absolute de -5 = " + Math.abs(-5));
		
		System.out.println("El entero mas chico mayor 9.3 = " + Math.ceil(9.3)); //devuelve el entero mas chico mayor al parametro
		System.out.println("El entero mas chico menor 9 = " + Math.floor(9.4)); //devuelve el entero mas chico menor al parametro
		
		//Funciones Trigonometricas
		System.out.println("\ncos(0) = " + Math.cos(0));
		System.out.println("sin(0) = " + Math.sin(0));
		System.out.println("tan(0) = " + Math.tan(0));
		
		System.out.println("\nlog(e) = " + Math.log(Math.E));
		
		System.out.println("\nEl maximo entre 2 y 5 = " + Math.max(2, 5));
		System.out.println("El minimo entre 2 y 5 = " + Math.min(2, 5));
		
		//Para crear un numero seudo-aleatorio se debe crear una clase
		SecureRandom sran = new SecureRandom();
		System.out.println("\nNumero random en un rango de (1,50): " + sran.nextInt(20));
		
		//-----------------------------------------------------STRINGS-----------------------------------------------------//
		System.out.println("\nStrings");
		//Tipos de instanciacion
		String cadena = "Esto es un string";
		String cadena2 = new String("Hola Mundo!");
		
		//Recorrer un string
		System.out.print("Cadena recorrida: ");
		for (int i = 0; i < cadena.length(); i++) {
			System.out.print(cadena.charAt(i));
		}
		
		System.out.println("");
		System.out.print("Cadena recorrida con for mejorado: ");
		for (char str : cadena2.toCharArray()) {
			System.out.print(str);
		}
		
		//Obtener caracteres dentro de un rango indicado
		System.out.println("");
		System.out.print("Extraemos caracteres en rango de (0,4): ");
		System.out.println(cadena.substring(0,4));
		
		//comparar cadenas
		System.out.println("la cadena2 = 'Hola Mundo!'?: " + cadena2.equals("Hola Mundo!"));
		//comparar cadenas ignorando las mayusculas
		System.out.println("la cadena2 = 'HOLA MUNDO!'?: " + cadena2.equalsIgnoreCase("HOLA MUNDO!"));
	}

}
