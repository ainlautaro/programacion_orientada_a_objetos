//ejemplo, de lanzamiento, propagacion de una excepcion, y obtencion de informacion adicional

public class LimpiezaPilaExcepciones {

	public static void main(String[] args) {
		
		try {
			metodo1();
		}catch(Exception e) {
			System.err.printf("%s%n%n", e.getMessage());
			e.printStackTrace();
			
			StackTraceElement[] elementosRastreo = e.getStackTrace();
			
			System.out.printf("%nRastreo de la pila de getStackTrace:%n");
			System.out.printf("Clase\t\t\tArchivo\t\t\t\tLinea\tMetodo%n");
		
			for (StackTraceElement elemento : elementosRastreo) {
				System.out.printf("%s\t", elemento.getClassName());
				System.out.printf("%s\t", elemento.getFileName());
				System.out.printf("%s\t", elemento.getLineNumber());
				System.out.printf("%s%n", elemento.getMethodName());
			}
		}

	}
	
	//metodos donde se produciran las excepciones
	public static void metodo1() throws Exception{
		metodo2();
	}
	public static void metodo2() throws Exception{
		metodo3();
	}
	public static void metodo3() throws Exception{
		throw new Exception("La excepcion se lanzo en el metodo3");
	}

}
