import java.util.InputMismatchException;
import java.util.Scanner;

public class EjemploExcepciones {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		boolean ContinuarCiclo = true;
		
		//con do try controlamos todas las excepciones que pueden llegar a ocurrir
		//dentro de ese bloque de codigo
		do {
			try {
			
				//Ejemplo de excepciones sin manejar
				System.out.print("Introduzca un numerador entero: ");
				int numerador = input.nextInt();
				System.out.print("Introduzca un denominador entero: ");
				int denominador = input.nextInt();
				
				int resultado = cociente(numerador, denominador);
				System.out.printf("%nResultado: %d/%d = %d%n", numerador, denominador, resultado);
				
				ContinuarCiclo = false;
//			}catch(InputMismatchException inputMismatchException) {
//				System.err.printf("%n Excepcion: %s%n", inputMismatchException);
//				input.nextLine();
//				System.out.printf("Debe introducir enteros, intente de nuevo. %n");
//				
//			}catch(ArithmeticException ArithmeticException) {
//				System.err.printf("%n Excepcion: %s%n", ArithmeticException);
//				input.nextLine();
//				System.out.printf("No se puede realizar division por 0 %n");
				
			//con catch(nombreExcepcion), lo que este dentro son las instrucciones que se ejecutan
			//para controla la excepcion
			}catch(InputMismatchException | ArithmeticException e){
				System.err.printf("%n Excepcion: %s%n", e);
				input.nextLine();
				System.out.printf("No se puede realizar division por 0 %n");				
			}
			finally {
				System.out.println("Lo que esta dentro del bloque 'finally' se ejecuta siempre");
			}
			
		}while(ContinuarCiclo);
	}

	public static int cociente(int numerador, int denominador) {
		return numerador/denominador;
	}
	
}
