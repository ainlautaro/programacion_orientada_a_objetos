import java.util.Scanner;

//ejemplo de verificacion del estado de las variables en un momento dado, sino se cumple salta una excepcion
//los asserts solo deben usarse para debugear, no deberian estar a la hora de que el usuario utilice el programa
public class PruebasAssert {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Ingrese un numero entre 1 y 10: ");
		int num = input.nextInt();
		
		assert (num > 0 && num <= 10) : "Numero incorrecto " + num;
		System.out.printf("Usted escribio %d%n", num);

	}

}
