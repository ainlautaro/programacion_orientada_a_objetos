import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class EjemploJOptionPane {

	public static void main(String[] args) {
		
		UIManager.put("OptionPane.minimumSize",new Dimension(350, 130)); //Seteo la dimension de la ventana
		
		
	    String[] items = {"Masculino", "Femenino", "Otro"}; //Opciones que van a salir en el
	    JComboBox sexo = new JComboBox(items);
	    
	    JTextField field1 = new JTextField("Nombre");
	    JTextField field2 = new JTextField("Apellido");
	    JTextField field3 = new JTextField("19");
	    
	    JPanel panel = new JPanel(new GridLayout(0, 2));
	    
	    //Label Input
	    panel.add(new JLabel("Nombre:"));panel.add(field1);
	    panel.add(new JLabel("Apellido:"));panel.add(field2);
	    panel.add(new JLabel("Edad:"));panel.add(field3);
	    panel.add(new JLabel("Sexo:"));panel.add(sexo);
	    
	    //modifico el nombre de la venta
	    int result = JOptionPane.showConfirmDialog(null, panel, "Sistema de Registro Paciente",
	        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	   
	    //Compruebo si se enviaron los datos
	    if (result == JOptionPane.OK_OPTION) {
	        System.out.println(field1.getText()+ " " + field2.getText()+ " " + field3.getText() + " " + sexo.getSelectedItem());
	    }
		
		
	}

}
