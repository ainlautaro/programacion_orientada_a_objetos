public class EmpleadoPorComision extends Empleado{

	private Double ventasBrutas, tarifaComision;
	
	public EmpleadoPorComision(String nombre, String apellido, String dni, Double ventasBrutas, Double tarifaComision) {
		super(nombre, apellido, dni);
		this.ventasBrutas = ventasBrutas;
		this.tarifaComision = tarifaComision;
	}
	
	public Double ingresos(){
		return getVentasBrutas() * getTarifaComision();
	}

	@Override
	public String toString() {
		return  "Empleado por Comision: " + super.toString() +
				"\n Ventas Brutas: $" + getVentasBrutas() +
				"\n Comision: " + getTarifaComision();
	}

	public final Double getVentasBrutas() {
		return ventasBrutas;
	}

	public final void setVentasBrutas(Double ventasBrutas) {
		this.ventasBrutas = ventasBrutas;
	}

	public final Double getTarifaComision() {
		return tarifaComision;
	}

	public final void setTarifaComision(Double tarifaComision) {
		this.tarifaComision = tarifaComision;
	}

}
