
public class EmpleadoPorHoras extends Empleado{

	private Double sueldo, horas;

	public EmpleadoPorHoras(String nombre, String apellido, String dni, Double sueldo, Double horas) {
		super(nombre, apellido, dni);
		
		this.sueldo = sueldo;
		this.horas = horas;
		
	}
	
	public Double ingresos() {
		if (getHoras()<=40) {
			return getSueldo()*getHoras();
		}else {
			return 40 * getSueldo() + (getHoras() - 40) * getSueldo() * 1.5;
		}
	}

	@Override
	public String toString() {
		return "Empleado Por Horas: " + super.toString() + "\n Sueldo: $" + getSueldo() + ", horas: " + getHoras();
	}

	public Double getSueldo() {
		return sueldo;
	}

	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}

	public Double getHoras() {
		return horas;
	}

	public void setHoras(Double horas) {
		this.horas = horas;
	}	
	
}
