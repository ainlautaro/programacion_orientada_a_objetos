//Las clases abstractas no se pueden instanciar, pero permiten generar metodos "compartamientos"
//que van a compartir los objetos

public abstract class Empleado {

	private String nombre, apellido, dni;

	public Empleado(String nombre, String apellido, String dni) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}

	public abstract Double ingresos();
	
	@Override
	public String toString() {
		return getNombre()+" "+getApellido()+" "+" \n DNI: " + getDni()+ "\n Ingresos: " + ingresos();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
}