import java.util.ArrayList;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class Main {

	public static void main(String[] args) {
		
		EmpleadoAsalariado empleadoAsalariado = new EmpleadoAsalariado("Ain", "Garcia", "40205760", 5000.0);
		EmpleadoPorHoras empleadoPorHoras = new EmpleadoPorHoras("Sebastian", "Cereminati", "37878505", 3000.0, 40.0);
		EmpleadoPorComision empleadoPorComision = new EmpleadoPorComision("Lautaro", "Nahuelanca", "42202761", 300000.0, 0.1);
		EmpleadoBaseMasComision empleadoBaseMasComision = new EmpleadoBaseMasComision("Alan", "Uribe", "44872515", 300000.0, 0.1, 50000.0);
		
		System.out.println("Empleados procesados por separado: ");
		
		System.out.printf("%s %n%n %s %n%n %s %n%n %s %n",
						 empleadoAsalariado,
						 empleadoPorHoras,
						 empleadoPorComision,
						 empleadoBaseMasComision);
		
		System.out.println("\nEmpleados procesados de forma polimorfica:");
		
		ArrayList<Empleado> empleados = new ArrayList<>();
		empleados.add(empleadoAsalariado);
		empleados.add(empleadoPorHoras);
		empleados.add(empleadoPorComision);
		empleados.add(empleadoBaseMasComision);
		
		for (Empleado empleado : empleados) {
			System.out.println("\n"+empleado);
			
			if (empleado instanceof EmpleadoBaseMasComision) {
				EmpleadoBaseMasComision empleadobmc = (EmpleadoBaseMasComision) empleado;
				System.out.printf(" El nuevo salario base con 10%% de aumento es: $%,.2f%n", empleadobmc.getSalarioBase());
			}
		}
		
		for (int i = 0; i < empleados.size(); i++) {
			System.out.printf("\n El empleado %d es un %s%n", (i+1), empleados.get(i).getClass().getName());
		}
	}

}
