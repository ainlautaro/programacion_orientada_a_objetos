import java.security.SecureRandom;

public class ProductoUno {

	private RecursoCompartido rc;
	
	public ProductoUno(RecursoCompartido rc) {
		super();
		this.rc = rc;
	}
	
	public void run() {
		SecureRandom r = new SecureRandom();
		this.getRc().produccionNro(r.nextInt(10));
	}
	
	public RecursoCompartido() {
		return rc;
	}
	
	public void setRc(RecursoCompartido rc) {
		this.rc = rc;
	}

	public RecursoCompartido getRc() {
		return rc;
	}

}
